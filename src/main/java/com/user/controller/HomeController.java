package com.user.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.user.dao.UserinfoDao;
import com.user.model.Userinfo;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	UserinfoDao userinfoDao;
	
	public UserinfoDao getUserinfoDao() {
		return userinfoDao;
	}

	@Autowired
	public void setUserinfoDao(UserinfoDao userinfoDao) {
		this.userinfoDao = userinfoDao;
	}
	
	@RequestMapping(value = "/add.htm", method = RequestMethod.POST)
	public ModelAndView add(Userinfo user) throws Exception {
		userinfoDao.saveUserinfo(user);
		return new ModelAndView("redirect:list.htm");
	}

	@RequestMapping(value = {"/", "list.htm"}, method = RequestMethod.GET)
	public ModelAndView list() throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("userList", userinfoDao.getAllUserinfo());
		modelMap.addAttribute("user", new Userinfo());
		return new ModelAndView("userForm", modelMap);		
	}
	
	@RequestMapping(value = "edit.htm", method = RequestMethod.GET)	
	public ModelAndView edit(@RequestParam("id") String id) throws Exception {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("user", userinfoDao.findById(id));
		return new ModelAndView("edit", modelMap);		
	}
	
	@RequestMapping(value = "update.htm", method = RequestMethod.POST)
	public ModelAndView update(@ModelAttribute("user") Userinfo user) throws Exception {
		userinfoDao.updateUserinfo(user);
		return new ModelAndView("redirect:list.htm");
	}
	
	@RequestMapping(value = "delete.htm")
	public ModelAndView update(@RequestParam("id") String id) throws Exception {
		userinfoDao.deleteUserinfo(userinfoDao.findById(id));
		return new ModelAndView("redirect:list.htm");
	}
}