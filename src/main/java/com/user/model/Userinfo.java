package com.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USERINFO")
public class Userinfo implements Comparable<Userinfo>, Serializable {
	private static final long serialVersionUID = -6027340279914938533L;

	private String userName;
	private String id;
	private String workspaceId;
	private String wsDomain;

	@Column(name = "ws_domain")
	public String getWsDomain() {
		return wsDomain;
	}

	public void setWsDomain(String wsDomain) {
		this.wsDomain = wsDomain;
	}

	@Column(name = "Workspace_id")
	public String getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(String workspaceId) {
		this.workspaceId = workspaceId;
	}

	@Id
	@Column(name = "USER_ID")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "USERNAME")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof Userinfo && id == ((Userinfo) o).id;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	public int compareTo(Userinfo i) {
		int c = userName.compareTo(i.userName);
		return (c == 0 ? id.compareTo(i.id) : c);
	}

}