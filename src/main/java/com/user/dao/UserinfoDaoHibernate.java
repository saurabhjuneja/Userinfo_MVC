package com.user.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.classic.Session;
import org.hibernate.SessionFactory;

import com.user.model.Userinfo;

import java.util.List;

@Repository
public class UserinfoDaoHibernate implements UserinfoDao {
	private SessionFactory sessionFactory;

	@Autowired
	public UserinfoDaoHibernate(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session currentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public List<Userinfo> getAllUserinfo() {
		List<Userinfo> l = currentSession().find("from Userinfo where length(user_id) < 32");
		return l;
	}

	public Userinfo findById(String Id) {
		Userinfo u = (Userinfo) currentSession().get(
				com.user.model.Userinfo.class, Id);
		return u;
	}
	
	public void updateUserinfo(Userinfo u){
		currentSession().merge(u);
	}

	public void saveUserinfo(Userinfo userinfo) {
		currentSession().save(userinfo);
	}

	public void deleteUserinfo(Userinfo userinfo) {
		currentSession().delete(userinfo);
	}
}