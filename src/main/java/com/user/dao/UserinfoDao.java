package com.user.dao;

import com.user.model.Userinfo;

import java.util.List;

import org.springframework.stereotype.Repository;

public interface UserinfoDao {
	public Userinfo findById(String Id);
	public void saveUserinfo(Userinfo userinfo);
	public void deleteUserinfo(Userinfo userinfo);
	public void updateUserinfo(Userinfo userinfo);
	public List<Userinfo> getAllUserinfo();
}