<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Registration Page</title>
</head>
<body>

<form:form id="saurabh" action="add.htm" commandName="user">
		<table>
			<td><form:input path="id" type = "hidden" value = "<%=(long) (Math.random()*1000000000) %>"/></td>
		<tr>
			<td>User Name :</td>
			<td><form:input path="userName" /></td>
		</tr>
		<tr>
			<td>Workspace Id :</td>
			<td><form:input path="workspaceId" /></td>
		</tr>
		<tr>
			<td>Workspace Domain :</td>
			<td><form:input path="wsDomain" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Register"></td>
		</tr>
	</table>
</form:form>
<c:if test="${fn:length(userList) > 0}">
	<table cellpadding="5">
		<tr class="even">
			<th>Id</th>
			<th>User Name</th>
			<th>Workspace Id</th>
			<th>Workspace Domain</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${userList}" var="user" varStatus="status">
			<tr class="<c:if test="${status.count % 2 == 0}">even</c:if>">
				<td>${user.id}</td>
				<td>${user.userName}</td>
				<td>${user.workspaceId}</td>
				<td>${user.wsDomain}</td>
				<td><a href="edit.htm?id=${user.id}">Edit</a></td>
				<td><a href="delete.htm?id=${user.id}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>
</body>
</html>