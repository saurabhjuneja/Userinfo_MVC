<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.even {
	background-color: silver;
}
</style>
<title>Registration Page</title>
</head>
<body>

<form:form action="update.htm" commandName="user" method="POST" >
		<table>
		    <td></td>
			<td><form:hidden path="id"/></td>
		<tr>
			<td>User Name :</td>
			<td><form:input path="userName" /></td>
		</tr>
		<tr>
			<td>Workspace Id :</td>
			<td><form:input path="workspaceId" /></td>
		</tr>
		<tr>
			<td>Workspace Domain :</td>
			<td><form:input path="wsDomain" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Save"></td>			
		</tr>
	</table>
</form:form>
</body>
</html>